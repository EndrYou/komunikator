﻿using Pogadamy.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pogadamy.ViewModels
{
    public class HomeVM
    {
        public IEnumerable<Chat> Chats { get; set; }
        public IEnumerable<Message> Messages { get; set; }
        public Message Message { get; set; }
        public string CurrentlyChatting { get; set; }
        public int MaxMessages { get; set; }
        public int howManyNow { get; set; }
    }
}
