﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pogadamy.ViewModels
{
    public class UserRolesMV
    {
        public string Username { get; set; }
        public IEnumerable<string> UserRoles { get; set; }
    }
}
