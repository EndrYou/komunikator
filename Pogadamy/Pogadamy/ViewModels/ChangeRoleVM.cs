﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pogadamy.ViewModels
{
    public class ChangeRoleVM
    {
        public string Username { get; set; }
        public IQueryable<IdentityRole> Roles { get; set; }
    }
}
