﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Pogadamy.ViewModels
{
    public class CreateRoleVM
    {
        [Required(ErrorMessage = "Nazwa roli jest wymagana")]
        public string RoleName { get; set; }
    }
}
