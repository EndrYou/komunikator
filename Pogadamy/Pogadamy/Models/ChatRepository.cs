﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pogadamy.Models
{
    public class ChatRepository : IChatRepository
    {
        private readonly AppDbContext _appDbContext;
        public ChatRepository(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }

        public void CreateChat(Chat chat)
        {
            _appDbContext.Chats.Add(chat);
            _appDbContext.SaveChanges();
        }

        public bool IsChat(string firstUser, string secondUser)
        {
            var result = _appDbContext.Chats.Any(c => (c.FirstUserName == firstUser || c.SecondUserName == firstUser) && (c.FirstUserName == secondUser || c.SecondUserName == secondUser));

            return result;  
        }

        public IEnumerable<Chat> GetChats(string username)
        {
            var chats = _appDbContext.Chats.Where(c => c.FirstUserName == username || c.SecondUserName == username).OrderByDescending(d => d.LastMessageDate);

            return chats;
        }
        
        public int GetChatId(string firstUser, string secondUser)
        {
            if (firstUser != null && secondUser != null)
            {
                int chatId = _appDbContext.Chats.FirstOrDefault(c => (c.FirstUserName == firstUser || c.SecondUserName == firstUser) && (c.FirstUserName == secondUser || c.SecondUserName == secondUser)).Id;
                return chatId;
            }

            return 0;
            
        }
    }
}
