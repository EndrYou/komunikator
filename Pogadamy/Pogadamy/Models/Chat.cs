﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Pogadamy.Models
{
    public class Chat
    {
        public int Id { get; set; }

        [MaxLength(50)]
        public string FirstUserName { get; set; }

        [MaxLength(50)]
        public string SecondUserName { get; set; }
        public DateTime LastMessageDate { get; set; }
    }
}
