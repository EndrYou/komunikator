﻿using Pogadamy.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pogadamy.Models
{
    public interface IMessagesRepository
    {
        IEnumerable<Message> GetMessages(int chatId, int howMany);
        void AddMessage(Message message);
        int MaxMessages(int userId);
    }
}
