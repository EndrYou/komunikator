﻿using System.Collections.Generic;
using System.Linq;

namespace Pogadamy.Models
{
    public class MessagesRepository : IMessagesRepository
    {
        private readonly AppDbContext _appDbContext;
        public MessagesRepository(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }

        public void AddMessage(Message message)
        {
            _appDbContext.Messages.Add(message);
            var chat = _appDbContext.Chats.FirstOrDefault(x => x.Id == message.ChatId);
            chat.LastMessageDate = message.SendTimeMessage;
            _appDbContext.Update(chat);

            _appDbContext.SaveChanges();
        }

        public IEnumerable<Message> GetMessages(int chatId, int howMany)
        {
            var messages = _appDbContext.Messages.Where(m => m.ChatId == chatId).OrderByDescending(m => m.SendTimeMessage).Take(howMany);

            return messages;
        }

        public int MaxMessages(int chatId)
        {
            return _appDbContext.Messages.Where(m => m.ChatId == chatId).Count();
        }
    }
}
