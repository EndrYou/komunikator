﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Pogadamy.Models
{
    public class Message
    {
        public int Id { get; set; }
        
        [Required]
        [MaxLength(200)]
        public string TextContent { get; set; }

        [MaxLength(50)]
        public string SenderName { get; set; }
        public DateTime SendTimeMessage { get; set; }
        public bool NewMessage { get; set; }
        public bool HadRead { get; set; }
        public int ChatId { get; set; }
    }
}
