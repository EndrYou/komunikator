﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pogadamy.Models
{
    public interface IChatRepository
    {
        void CreateChat(Chat chat);
        bool IsChat(string firstUser, string secondUser);
        IEnumerable<Chat> GetChats(string username);
        int GetChatId(string firstUser, string secondUser = null);
    }
}
