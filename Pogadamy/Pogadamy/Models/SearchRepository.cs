﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pogadamy.Models
{
    public class SearchRepository : ISearchRepository
    {
        private readonly AppDbContext _appDbContext;
        public SearchRepository(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }
        public IEnumerable<IdentityUser> SearchUser(string username)
        {
            var user = _appDbContext.Users.Where(u => u.UserName.Contains(username));
            return user;
        }
    }
}
