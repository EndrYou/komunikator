﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pogadamy.Models
{
    public interface ISearchRepository
    {
        IEnumerable<IdentityUser> SearchUser(string username);
    }
}
