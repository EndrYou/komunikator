﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Pogadamy.Migrations
{
    public partial class updateChatmodel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FirstUserId",
                table: "Chats");

            migrationBuilder.DropColumn(
                name: "SecondUserId",
                table: "Chats");

            migrationBuilder.AddColumn<string>(
                name: "FirstUserName",
                table: "Chats",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastMessageDate",
                table: "Chats",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "SecondUserName",
                table: "Chats",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FirstUserName",
                table: "Chats");

            migrationBuilder.DropColumn(
                name: "LastMessageDate",
                table: "Chats");

            migrationBuilder.DropColumn(
                name: "SecondUserName",
                table: "Chats");

            migrationBuilder.AddColumn<int>(
                name: "FirstUserId",
                table: "Chats",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "SecondUserId",
                table: "Chats",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
