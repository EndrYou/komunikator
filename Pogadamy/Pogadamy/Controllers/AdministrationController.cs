﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Pogadamy.Models;
using Pogadamy.ViewModels;

namespace Pogadamy.Controllers
{
    [Authorize]
    public class AdministrationController : Controller
    {
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly ISearchRepository _searchRepository;
        private readonly UserManager<IdentityUser> _userManager;

        public AdministrationController(RoleManager<IdentityRole> roleManager, ISearchRepository searchRepository, UserManager<IdentityUser> userManager)
        {
            _roleManager = roleManager;
            _searchRepository = searchRepository;
            _userManager = userManager;
        }

        public IActionResult Index()
        {
            if(TempData["ConfirmCreatRole"] != null && (bool)TempData["ConfirmCreatRole"] == true)
            {
                TempData["ConfirmCreatRole"] = true;
                return View();
            }

            if (TempData["ChangeRoleSucces"] != null && (bool)TempData["ChangeRoleSucces"] == true)
            {
                TempData["ChangeRoleSucces"] = true;
                return View();
            }

            if (TempData["RemoveRoleSucces"] != null && (bool)TempData["RemoveRoleSucces"] == true)
            {
                TempData["RemoveRoleSucces"] = true;
                return View();
            }

            TempData["ConfirmCreatRole"] = false;
            TempData["ChangeRoleSucces"] = false;
            TempData["RemoveRoleSucces"] = false;
            return View();

        }

        [HttpGet]
        public IActionResult CreateRole()
        {
            TempData["ConfirmCreatRole"] = false;
            return View();
        }


        [HttpPost]
        public async Task<IActionResult> CreateRole(CreateRoleVM createRoleVM)
        {
            if (ModelState.IsValid)
            {
                IdentityRole identityRole = new IdentityRole
                {
                    Name = createRoleVM.RoleName
                };

                var result = await _roleManager.CreateAsync(identityRole);

                if (result.Succeeded)
                {
                    TempData["ConfirmCreatRole"] = true;
                    return RedirectToAction("Index", "Administration");
                }

                foreach(var error in result.Errors)
                {
                    ModelState.AddModelError("", error.Description);
                }
            }

            return View(createRoleVM);
        }

        public IActionResult SearchUserToChangeRole()
        {
            return View();
        }

        [HttpPost]
        public IActionResult SearchUserToChangeRole(string username)
        {
            var result = _searchRepository.SearchUser(username);

            return View(result);
        }

        public IActionResult ChangeRole(string username)
        {
            TempData["ChangeRoleSucces"] = false;

            var roles = _roleManager.Roles;

            var changeRoleVM = new ChangeRoleVM()
            {
                Username = username,
                Roles = roles
            };

            return View(changeRoleVM);
        }

        [HttpPost]
        public async Task<IActionResult> ChangeRole(string username, string roleName)
        {
            if (!String.IsNullOrEmpty(roleName))
            {
                var user = await _userManager.FindByNameAsync(username);
                var isInRole = await _userManager.IsInRoleAsync(user, roleName);
                if (!isInRole)
                {
                    var result = await _userManager.AddToRoleAsync(user, roleName);
                    if (result.Succeeded)
                    {
                        TempData["ChangeRoleSucces"] = true;
                        return RedirectToAction("Index", "Administration");
                    }
                }
            }

            return RedirectToAction("ChangeRole", "Administration", new { username = username.ToString() });
        }

        public async Task<IActionResult> AllUserRoles(string username)
        {
            var user = await _userManager.FindByNameAsync(username);
            var roles = await _userManager.GetRolesAsync(user);

            var userRoles = new UserRolesMV()
            {
                Username = username,
                UserRoles = roles
            };

            return View(userRoles);
        }

        public IActionResult SearchUserToDeleteRole()
        {
            return View();
        }

        [HttpPost]
        public IActionResult SearchUserToDeleteRole(string username)
        {
            var result = _searchRepository.SearchUser(username);

            return View(result);
        }

        public async Task<IActionResult> UserRolesToDelete(string username)
        {
            TempData["RemoveRoleSucces"] = false;

            var user = await _userManager.FindByNameAsync(username);
            var roles = await _userManager.GetRolesAsync(user);

            var userRoles = new UserRolesMV()
            {
                Username = username,
                UserRoles = roles
            };

            return View(userRoles);
        }

        [HttpPost]
        public async Task<IActionResult> UserRolesToDelete(string username, string role)
        {
            var user = await _userManager.FindByNameAsync(username);
            var result = await _userManager.RemoveFromRoleAsync(user, role);
            if (result.Succeeded)
            {
                TempData["RemoveRoleSucces"] = true;
                return RedirectToAction("Index", "Administration","USUNIETE");
            }

            return RedirectToAction("UserRolesToDelete", "Administration", new { username = username.ToString() });
        }
    }
}
