﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Pogadamy.Models;
using Pogadamy.ViewModels;
using System;
using System.Linq;

namespace Pogadamy.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly IChatRepository _chatRepository;
        private readonly IMessagesRepository _messagesRepository;
        private readonly UserManager<IdentityUser> _userManager;

        public HomeController(IChatRepository chatRepository, IMessagesRepository messagesRepository, UserManager<IdentityUser> userManager)
        {
            _chatRepository = chatRepository;
            _messagesRepository = messagesRepository;
            _userManager = userManager;
        }
        public IActionResult Index(string chatuser = null, int howMany = 20)
        {
            if (howMany < 20)
                howMany = 20;

            TempData["chatUser"] = chatuser;
            TempData["howMany"] = howMany;
            var user = _userManager.GetUserName(User);
            var chats = _chatRepository.GetChats(user);
            int chatId = _chatRepository.GetChatId(user, chatuser);
            var messages = _messagesRepository.GetMessages(chatId, howMany);
            int maxMessages = _messagesRepository.MaxMessages(chatId);
            
            var homeVM = new HomeVM()
            {
                Chats = chats,
                Messages = messages,
                CurrentlyChatting = chatuser,
                MaxMessages = maxMessages,
                howManyNow = howMany
            };

            return View(homeVM);
        }

        [HttpPost]
        public IActionResult Index(HomeVM homeVM)
        {
            if (homeVM.Message.TextContent != null)
            {
                var user = _userManager.GetUserName(User);
                int chatId = _chatRepository.GetChatId(user, TempData["chatUser"].ToString());

                var message = new Message()
                {
                    TextContent = homeVM.Message.TextContent,
                    ChatId = chatId,
                    SendTimeMessage = DateTime.Now,
                    HadRead = true,
                    NewMessage = true,
                    SenderName = user
                };

                _messagesRepository.AddMessage(message);
            }

            return RedirectToAction("Index", new { chatuser = TempData["chatUser"].ToString() });
        }

        public IActionResult CreateChat(string username)
        {
            var firstUserName = _userManager.GetUserName(User);
            TempData["chatUser"] = username;

            var chat = new Chat()
            {
                FirstUserName = firstUserName,
                SecondUserName = username,
                LastMessageDate = DateTime.Now
            };

            if(!_chatRepository.IsChat(firstUserName, username))
            {
                _chatRepository.CreateChat(chat);
            }

            return RedirectToAction("Index", new { chatuser = TempData["chatUser"].ToString() });
        }

        [HttpPost]
        public IActionResult ShowMoreMessage()
        {
            int howMany = (int)TempData["howMany"];
            howMany += 10;
            return RedirectToAction("Index", new { chatuser = TempData["chatUser"].ToString(), howMany});
        }
    }
}
